<?php

namespace Sharent\Libraries\SupportLogic\Providers;

use Illuminate\Support\ServiceProvider;

class PropertyScoreServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //Publish Config
        if (!file_exists(config_path('beComeAHost.php'))) {
            $this->publishes([
                __DIR__ . '/config/beComeAHost.php' => config_path('beComeAHost.php'),
            ], 'config');
        }

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
