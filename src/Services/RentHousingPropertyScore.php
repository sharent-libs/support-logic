<?php

namespace Sharent\Libraries\SupportLogic\Services;

use Sharent\Libraries\SupportLogic\Support\InterfacePropertyStep;

class RentHousingPropertyScore implements InterfacePropertyStep
{
    private $entity;

    public function __construct($becomeAHostEntity)
    {
        $this->entity = $becomeAHostEntity;
    }

    /**
     * 第一步：選擇房源類型
     * @return bool
     */
    public function firstMilestoneIsCompleted()
    {
        if ($this->entity->kind === null) {
            return false;
        }
        if ($this->entity->state === null) {
            return false;
        }
        if ($this->entity->identity === null) {
            return false;
        }
        if (collect($this->entity->shape)->isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * 第二步：輸入房源地址
     * @return bool
     */
    public function secondMilestoneIsCompleted()
    {
        //檢查主表property有無資料
        if (collect($this->entity->rent)->isEmpty()) {
            return false;
        }

        if (!collect($this->entity->address)->keys()->contains('city')) {
            return false;
        }

        if (!collect($this->entity->address)->keys()->contains('region')) {
            return false;
        }

        if (!collect($this->entity->address)->keys()->contains('address')) {
            return false;
        }

        if (collect($this->entity->floor_info)->isEmpty()) {
            return false;
        }

        if ($this->entity->lat === null) {
            return false;
        }
        if ($this->entity->lng === null) {
            return false;
        }
        return true;
    }

    /**
     * 第三步：房源基本資訊
     * @return bool
     */
    public function thirdMilestoneIsCompleted()
    {
        //價錢
        if ($this->entity->rent === null) {
            return false;
        }

        if (!collect($this->entity->rent)->keys()->contains('price')) {
            return false;
        }

        //最短租期
        if (collect($this->entity->rent_interval)->isEmpty()) {
            return false;
        }
//        if (!property_exists($this->entity->other_fees, 'rent_interval')) {
//            return false;
//        }
        //押金
        if (collect($this->entity->deposit)->isEmpty()) {
            return false;
        }
        //房間數量資訊
        if (collect($this->entity->room)->isEmpty()) {
            return false;
        }

        return true;
    }

    /**
     * 第四步：房源標題與描述
     * @return bool
     */
    public function fourthMilestoneIsCompleted()
    {
        if ($this->entity->meta === null) {
            return false;
        }

        if (!collect($this->entity->meta)->keys()->contains('title')) {
            return false;
        }

        if (!collect($this->entity->meta)->keys()->contains('description')) {
            return false;
        }

        return true;
    }

    /**
     * 第五步：家具、設備與其他
     * @return bool
     */
    public function fifthMilestoneIsCompleted()
    {
        if (collect($this->entity->amenities)->isEmpty()
            && collect($this->entity->electrical)->isEmpty()
            && collect($this->entity->furniture)->isEmpty()
            && collect($this->entity->rules)->isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * 第六步：房源照片上傳
     * @return bool
     */
    public function sixthMilestoneIsCompleted()
    {
        //房間數量資訊
        if (collect($this->entity->file)->isEmpty()) {
            return false;
        }
        return true;
    }

}
