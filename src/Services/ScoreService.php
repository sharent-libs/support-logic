<?php

namespace Sharent\Libraries\SupportLogic\Services;
use Sharent\Libraries\SupportLogic\Services\RentPropertyScore;
use Sharent\Libraries\SupportLogic\Services\BusinessPropertyScore;

class ScoreService
{
    /**
     * 執行的步驟數量 以及 可以上架的狀態
     * @var
     */
    private $beComeAHostConf;

    private $scoreService;

    public function __construct($becomeAHostEntity) {
        if(!isset($becomeAHostEntity->kind)){
            throw new \Exception('property kind not define');
        }

        $this->beComeAHostConf = config('beComeAHost.'.$becomeAHostEntity->kind);
        $serviceName = ($this->beComeAHostConf['serviceName'])?$this->beComeAHostConf['serviceName']:'RentHousingPropertyScore';
        if (!$serviceName){
            throw new \Exception("property kind: {$becomeAHostEntity->kind} not define, please check ur database migrate");
        }
        $className = "Sharent\\Libraries\\SupportLogic\\Services\\$serviceName";
        $this->scoreService = new $className($becomeAHostEntity);
    }

    public function getMilestones()
    {
        $milestones = [];
        $status = true;
        $milestones['completeScore'] = 0x00;
        //驗證完成度
        foreach ($this->beComeAHostConf['stepList'] as $stepKey => $require) {
            $func = $stepKey . 'MilestoneIsCompleted';
            $milestones['step'][$func] = $this->$func();
            if (! $milestones['step'][$func] and $require) {
                $status = false;
            }
            if ($milestones['step'][$func]){
                $milestones['completeScore'] = $milestones['completeScore'] | $this->beComeAHostConf['scoreRule'][$stepKey];
            }

        }
        $milestones['milestoneIsCompleted'] = $status;
        if($milestones['milestoneIsCompleted'] === true){
            $milestones['completeScore'] = $milestones['completeScore'] | $this->beComeAHostConf['scoreRule']['isComplete'];
        }

        $collection = collect($milestones['step']);
        $filtered = $collection->filter(function ($value, $key) {
            return $value == true;
        });
        $milestones['schedule'] = (int)(count($filtered)/count($this->beComeAHostConf['stepList']) * 100);
        return $milestones;
    }

    public function getScoreRule(){
        return $this->beComeAHostConf['scoreRule'];
    }

    public function firstMilestoneIsCompleted(){
        return $this->scoreService->firstMilestoneIsCompleted();
    }

    public function secondMilestoneIsCompleted(){
        return $this->scoreService->secondMilestoneIsCompleted();
    }

    public function thirdMilestoneIsCompleted(){
        return $this->scoreService->thirdMilestoneIsCompleted();
    }

    public function fourthMilestoneIsCompleted(){
        return $this->scoreService->fourthMilestoneIsCompleted();
    }

    public function fifthMilestoneIsCompleted(){
        return $this->scoreService->fifthMilestoneIsCompleted();
    }

    public function sixthMilestoneIsCompleted(){
        return $this->scoreService->sixthMilestoneIsCompleted();
    }
}
