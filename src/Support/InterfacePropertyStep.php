<?php

namespace Sharent\Libraries\SupportLogic\Support;

interface InterfacePropertyStep
{
    public function firstMilestoneIsCompleted();
    public function secondMilestoneIsCompleted();
    public function thirdMilestoneIsCompleted();
    public function fourthMilestoneIsCompleted();
    public function fifthMilestoneIsCompleted();
    public function sixthMilestoneIsCompleted();
}
