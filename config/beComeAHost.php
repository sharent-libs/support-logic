<?php

return [
    'rent-housing' => [
        'serviceName' => 'RentHousingPropertyScore',
        'scoreRule' => [
            'isComplete' => 0x01,
            'first' => 0x02,
            'second' => 0x04,
            'third' =>  0x08,
            'fourth' => 0x10,
            'fifth' => 0x20,
            'sixth' =>  0x1000,
        ],
        'stepList' =>[
            'first' => true,
            'second' => true,
            'third' => true,
            'fourth' => true,
            'fifth' => false,
            'sixth' => false,
        ]
    ],
    'rent-business' => [
        'serviceName' => 'RentBusinessPropertyScore',
        'scoreRule' => [
            'isComplete' => 0x01,
            'first' => 0x02,
            'second' => 0x04,
            'third' =>  0x08,
            'fourth' => 0x10,
            'fifth' => 0x1000,
            'sixth' =>  0x00,
        ],
        'stepList' =>[
            'first' => true,
            'second' => true,
            'third' => true,
            'fourth' => true,
            'fifth' => false,
            'sixth' => false,
        ]
    ],
    'sell-housing' => [
        'serviceName' => 'SellHousingPropertyScore',
        'scoreRule' => [
            'isComplete' => 0x01,
            'first' => 0x02,
            'second' => 0x04,
            'third' =>  0x08,
            'fourth' => 0x10,
            'fifth' => 0x1000,
            'sixth' =>  0x00,
        ],
        'stepList' =>[
            'first' => true,
            'second' => true,
            'third' => true,
            'fourth' => true,
            'fifth' => false,
            'sixth' => false,
        ]
    ],
    'sell-business' => [
        'serviceName' => 'SellBusinessPropertyScore',
        'scoreRule' => [
            'isComplete' => 0x01,
            'first' => 0x02,
            'second' => 0x04,
            'third' =>  0x08,
            'fourth' => 0x10,
            'fifth' => 0x1000,
            'sixth' =>  0x00,
        ],
        'stepList' =>[
            'first' => true,
            'second' => true,
            'third' => true,
            'fourth' => true,
            'fifth' => false,
            'sixth' => false,
        ]
    ],
];
